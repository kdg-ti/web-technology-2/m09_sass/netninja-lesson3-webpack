import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import CopyWebpackPlugin from 'copy-webpack-plugin';
import path from "path";

const dist = path.resolve("dist")

const config = {
    mode: "development",
    plugins: [
        new HtmlWebpackPlugin({template: "./src/index.html"}),
        new MiniCssExtractPlugin(),
        new CopyWebpackPlugin({patterns: [{from: 'src/img', to: 'img'}]})
    ],
    module: {
        rules: [
            {
                test: /\.s?css$/,
                //      use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/resource",
            },
            // Font assets
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: "asset",
            },
        ],
    },
    output: {
        clean: true
    },
    devServer: {
        contentBase: dist,
        open: true,
        overlay: true,
        compress: true
    }
};

export default config;